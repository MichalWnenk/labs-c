/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.zoo;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author MadMike
 */
public class CustomTracker implements ServiceTrackerCustomizer {
    
    private final BundleContext bc;
    
    public CustomTracker(BundleContext bc) {
        this.bc = bc;
    }

    @Override
    public Object addingService(ServiceReference sr) {
        Animal animal = (Animal) bc.getService(sr);
        Zoo.get().addAnimal(animal);
        System.out.println("Dodany został " + animal.getName());
        return animal;
    }

    @Override
    public void modifiedService(ServiceReference sr, Object o) {
    }

    @Override
    public void removedService(ServiceReference sr, Object o) {
        Animal animal = (Animal) o;
        Zoo.get().removeAnimal(animal);
        System.out.println("Usuniety został " + animal.getName());
    }
    
}
