/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal1.internal;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

/**
 *
 * @author MadMike
 */
public class Pterodaktyl implements Animal {
    
    private String status;
    private static PropertyChangeSupport sluchacz;
    private final String name;
    private final String species;
    
    public Pterodaktyl(){
        sluchacz = new PropertyChangeSupport(this);
        this.name = "Gucio";
        this.species = "Pterodaktyl";
        this.status = "Nic nie robi";
    }

    @Override
    public String getSpecies() {
       return species;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        sluchacz.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        sluchacz.removePropertyChangeListener(listener);
    }
    
}
