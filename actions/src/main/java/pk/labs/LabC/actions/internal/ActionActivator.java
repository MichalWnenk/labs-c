/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.actions.internal;

import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

/**
 *
 * @author MadMike
 */
public class ActionActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        
        AnimalAction lec = new AnimalAction() {
            
            @Override
            public String toString() {
                return "Lec";
            }

            @Override
            public boolean execute(Animal animal) {
                animal.setStatus("Lec");
                return true;
            }
        };
        
        AnimalAction biegnij = new AnimalAction() {
            
            @Override
            public String toString() {
                return "Biegnij";
            }

            @Override
            public boolean execute(Animal animal) {
                animal.setStatus("Biegnij");
                return true;
            }
        };
        
        AnimalAction jedz = new AnimalAction() {
            
            @Override
            public String toString() {
                return "Jedz lisc";
            }

            @Override
            public boolean execute(Animal animal) {
                animal.setStatus("Jedz lisc");
                return true;
            }
        };
        
        Hashtable property1 = new Hashtable();
        property1.put("species", "Pterodaktyl");
        property1.put("name", "lec");
        Hashtable property2 = new Hashtable();
        property2.put("species", "Tyranozaur");
        property2.put("name", "biegnij");
        Hashtable property3 = new Hashtable();
        property3.put("species", "Brontozaur");
        property3.put("name", "jedz");
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), lec, property1);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), biegnij, property2);
        bc.registerService(pk.labs.LabC.contracts.AnimalAction.class.getName(), jedz, property3);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    
}
