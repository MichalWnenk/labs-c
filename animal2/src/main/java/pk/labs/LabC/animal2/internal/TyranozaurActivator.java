/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabC.animal2.internal;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author MadMike
 */
public class TyranozaurActivator implements BundleActivator {
    
    @Override
    public void start(BundleContext bc) throws Exception {
        
        bc.registerService(Animal.class.getName(), new Tyranozaur(), null);
        Logger.get().log(this, "Tyranozaur biega w kolko :)");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
        Logger.get().log(this, "Tyranozaur przestał biegac i poszedł spac :)");
    }
    
   
    
}
